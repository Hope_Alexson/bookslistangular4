import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { BooksComponent } from './books.component';
import {
  ReactiveFormsModule,
  FormsModule
} from '@angular/forms';
@NgModule({
  declarations: [
    BooksComponent
  ],
  imports: [
    BrowserModule,
  ReactiveFormsModule,
  FormsModule
  ],
  providers: [],
  bootstrap: [BooksComponent]
})
export class AppModule { }
