import { Component } from '@angular/core';
import { BooksService } from './books.service';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'books',
  templateUrl: './books.component.html',
  styleUrls: ['../assets/css/bootstrap.min.css', './books.component.less'],
  providers: [BooksService]
})
export class BooksComponent {
  books = [];
  date = Number(new Date('January 01, 1800 00:00:00').getFullYear());
  pictures = [];
  img: string;
  title: string;
  autors: string;
  isbn: string;
  izdat: string;
  pages: number;
  tirazh: number;
  year: number;
  addBookForm: FormGroup;

  constructor(private booksService: BooksService, private fb: FormBuilder) {}

  addBookFormsubmit() {
    this.books.push({
      title: this.title,
      autors: this.autors,
      isbn: this.isbn,
      izdat: this.izdat,
      pages: this.pages,
      tirazh: this.tirazh,
      year: this.year,
      img: this.img
    });
  }

  ngOnInit() {
    this.books = this.booksService.books;
    this.initForm();
  }

  initForm() {
    this.addBookForm = this.fb.group({
      title: ['', [Validators.required, Validators.maxLength(30)]],
      autors: ['', [Validators.required, Validators.maxLength(20)]],
      isbn: ['', [Validators.required, isbnValidator]],
      izdat: ['', Validators.maxLength(30)],
      pages: ['', [Validators.required, Validators.min(1), Validators.maxLength(10000)]],
      tirazh: ['', [Validators.required, Validators.min(this.date)]],
      year: ['', [Validators.required, Validators.min(this.date)]],
      img: ['', [Validators.required]]
    });
  }

  class isbnValidator{

 isbn10(isbn: Array<number>): boolean {
  let revIsbn = isbn.reverse();
 
  const multiples = [];
  
  for(let i = 0; i<10; i++ ){
   multiples.push(revIsbn[i]*i);   
 }
  let multiplesSum = multiples.reduce(
    function(sum, current) {
        return sum + current;
    }, 0);
   if (multiplesSum % 11 === 0) {
     return true;
   } else {
     return false;
  }
}

 isbn13(isbn: Array<number>): boolean {
  
  const multiples = [];
  
  for(let i = 0; i<13; i++ ){
    if(i % 2 === 0){
    multiples.push(isbn[i]*1)
    } else {
        multiples.push(isbn[i]*3)
    }
 }
  
  let multiplesSum = multiples.reduce(
    function(sum, current) {
        return sum + current;}, 0);
 
   if (multiplesSum % 10 === 0) {
     return true;
   } else {
     return false;
  }
}

  validate(isbn: string): boolean {
    const rawISBN = isbn.split('-').join('').split('');

    const numISBN = [];

    for (let i = 0; i<rawISBN.length; i++){
        numISBN.push(parseInt(rawISBN[i]));
    }

    if (numISBN.length === 10) {
      return this.isbn10(numISBN);
    } else if (numISBN.length === 13){
      return this.isbn13(numISBN);
  } else {
      return false;
  }
}

  onEdit(item) {
    this.books = item;
  }

  onDelete(item) {
    for (let i = 0; i < this.books.length; i++) {
      if (item.id === this.books[i].id) {
        this.books.splice(i, 1);
        break;
      }
    }
  }

  /*  onClick(){
        if(this.book.id == 0){
        this.books.push({title: this.book.title, autors: this.book.autors, img: this.book.img, isbn: this.book.isbn, izdat: this.book.izdat, pages: this.book.pages, tirazh: this.book.tirazh, year: this.book.year});
        }
    }

 */
}

