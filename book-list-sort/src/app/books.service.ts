import { Injectable } from '@angular/core';

interface IBook {
  id: number;
  title: string;
  autors: string;
  pages: number;
  izdat?: string;
  year: number;
  tirazh: number;
  isbn: string;
  img?: string;
}

@Injectable()
export class BooksService {
  book: Array<IBook> = [];

  books: Array<IBook> = [
    {
      id: 0,
      title: 'Костры на алтарях',
      autors: 'Вадим Панов',
      pages: 475,
      izdat: 'Эксмо',
      year: 2008,
      tirazh: 2008,
      isbn: '978-5-699-25283-1',
      img:
        'https://cv1.litres.ru/static/bookimages/28/76/44/28764415.bin.dir/28764415.cover_330.jpg'
    },
    {
      id: 1,
      title: 'Черновик',
      autors: 'Сергей Лукьяненоко',
      pages: 414,
      izdat: 'Аст',
      year: 2005,
      tirazh: 2005,
      isbn: '5-17-033151-7',
      img:
        'https://cv0.litres.ru/static/bookimages/00/14/62/00146209.bin.dir/00146209.cover_330.jpg'
    },
    {
      id: 2,
      title: 'Дело Парадайна',
      autors: 'Роберт Хиченс',
      pages: 543,
      izdat: 'Центр Полиграф',
      year: 2003,
      tirazh: 2003,
      isbn: '5-9524-0589-4',
      img: ''
    },
    {
      id: 3,
      title: 'Cloud Atlas',
      autors: 'David Mitchel',
      pages: 529,
      izdat: 'Cayls LTD',
      year: 2012,
      tirazh: 2012,
      isbn: '978-1-444-73087-6',
      img: 'https://i.livelib.ru/boocover/1000217172/200/3f2e/Devid_Mitchell__Oblachnyj_atlas.jpg'
    },
    {
      id: 4,
      title: 'Вьюга теней',
      autors: 'Алексей пехов',
      pages: 539,
      izdat: 'Альфа-Книга',
      year: 2002,
      tirazh: 2002,
      isbn: '978-5-9922-0040-9',
      img: ''
    }
  ];

  addBook(book: IBook): void {
    this.books.push(book);
  }



}